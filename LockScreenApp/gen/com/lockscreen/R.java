/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.lockscreen;

public final class R {
    public static final class attr {
    }
    public static final class color {
        public static final int mycolor1=0x7f060000;
    }
    public static final class dimen {
        /**  Default screen margins, per the Android Design guidelines. 

         Customize dimensions originally defined in res/values/dimens.xml (such as
         screen margins) for sw720dp devices (e.g. 10" tablets) in landscape here.
    
         */
        public static final int activity_horizontal_margin=0x7f040000;
        public static final int activity_vertical_margin=0x7f040001;
    }
    public static final class drawable {
        public static final int droid=0x7f020000;
        public static final int droid1=0x7f020001;
        public static final int droidupdated=0x7f020002;
        public static final int home=0x7f020003;
        public static final int home1=0x7f020004;
        public static final int home128=0x7f020005;
        public static final int homeupdated=0x7f020006;
        public static final int ic_launcher=0x7f020007;
        public static final int images=0x7f020008;
        public static final int images0=0x7f020009;
        public static final int index=0x7f02000a;
        public static final int index1=0x7f02000b;
        public static final int lockicon=0x7f02000c;
        public static final int main=0x7f02000d;
        public static final int pc411logo=0x7f02000e;
        public static final int phone=0x7f02000f;
        public static final int phone150=0x7f020010;
        public static final int phone2=0x7f020011;
        public static final int phone3=0x7f020012;
        public static final int phoneupdated=0x7f020013;
        public static final int phoneupdated1=0x7f020014;
        public static final int tmforum=0x7f020015;
    }
    public static final class id {
        public static final int Button01=0x7f090008;
        public static final int action_settings=0x7f09000a;
        public static final int button1=0x7f090002;
        public static final int button2=0x7f090007;
        public static final int droid=0x7f090004;
        public static final int editText1=0x7f090001;
        public static final int home=0x7f090006;
        public static final int homelinearlayout=0x7f090005;
        public static final int ll_custom_lock_screen=0x7f090003;
        public static final int phone=0x7f090009;
        public static final int textView1=0x7f090000;
    }
    public static final class layout {
        public static final int activity_search=0x7f030000;
        public static final int activity_trouble_ticket=0x7f030001;
        public static final int custom_lock_screen=0x7f030002;
        public static final int main=0x7f030003;
        public static final int startwork=0x7f030004;
    }
    public static final class menu {
        public static final int search=0x7f080000;
        public static final int trouble_ticket=0x7f080001;
    }
    public static final class string {
        public static final int action_settings=0x7f050004;
        public static final int app_name=0x7f050001;
        public static final int hello=0x7f050000;
        public static final int hello_world=0x7f050005;
        public static final int title_activity_search=0x7f050003;
        public static final int title_activity_trouble_ticket=0x7f050006;
        public static final int unloack=0x7f050002;
    }
    public static final class style {
        public static final int Theme_Transparent=0x7f070000;
    }
}
