package com.lockscreen;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class StartWork extends Activity {
	public void onCreate(Bundle savedInstanceState) {

 	   super.onCreate(savedInstanceState);
 	   setContentView(R.layout.startwork);

	}

	public void launchSearchView(View v) {
		startActivity(new Intent(this, Search.class));
	}
	
	public void launchTroubleTicketView(View v) {
		startActivity(new Intent(this, TroubleTicket.class ));
	}
}
