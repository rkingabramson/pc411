package com.lockscreen;
import com.loopj.android.http.*;

public class RestClient {
		  private static final String BASE_URL = "http://env-4126955.jelastic.servint.net/";

		  private static AsyncHttpClient client = new AsyncHttpClient();

		  public static void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
		      client.get(getAbsoluteUrl(url), params, responseHandler);
		  }

		  public static void post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
		      client.post(getAbsoluteUrl(url), params, responseHandler);
		  }
		  
		  public static void delete(String url, AsyncHttpResponseHandler responseHandler){
			  client.delete(getAbsoluteUrl(url), responseHandler);
		  }

		  private static String getAbsoluteUrl(String relativeUrl) {
		      return BASE_URL + relativeUrl;
		  }
		
}